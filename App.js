import React from 'react';
import { Provider } from 'react-native-paper';
import Index from './src';
import { theme } from './src/core/theme';

const App = () => (
  <Provider theme={theme}>
    <Index />
  </Provider>
);

export default App;
