import { DefaultTheme } from 'react-native-paper';

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#483996',
    secondary: '#414757',
    error: '#f13a59',
  },
};
